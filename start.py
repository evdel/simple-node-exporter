import subprocess
import os

os.environ['PYTHONPATH'] = '/workspace'

subprocess.call('/app/.heroku/python/bin/gunicorn --bind 0.0.0.0:8080 app:app'.split(' '))