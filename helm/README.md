This api exposer enables to use the kubernetes API running on port 6443 (Rancher's default) from outside of the private network without having to open other ports than the standard 80 and 443.

How to use it :

Install helm, then

$ helm template . --values=values_production.yml

Or 

$ helm template . --values=values_sandbox.yml

