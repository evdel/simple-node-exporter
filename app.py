import psutil
from flask import Flask

app = Flask(__name__)

@app.route('/metrics')
def metrics():

    data = {
        'cpu': psutil.cpu_percent(),
        'ram': psutil.virtual_memory()[2],
        'disk': psutil.disk_usage("/")[3]

    }

    s = [f'{k} {v}' for k, v in data.items()]

    result = ""
    for item in s:
        result += item
        result += '\n'

    return result
